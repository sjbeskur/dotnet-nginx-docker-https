FROM microsoft/aspnetcore-build:2.0

RUN apt-get update 
#RUN apt-get upgrade -y

RUN apt-get install -y nginx vim curl

WORKDIR /tmp
RUN mkdir src/
ADD src/ src/

ADD gen-certs.sh .
ADD startup.sh .

#RUN bash gen-certs.sh

RUN rm /etc/nginx/sites-enabled/default
ADD etc/nginx/sites-enabled/my-https-cfg /etc/nginx/sites-enabled/

WORKDIR /tmp/src/
RUN dotnet publish --output /app
RUN cp /tmp/startup.sh /app

WORKDIR /app

#CMD [ "dotnet","tmp.dll", "--urls", "http://0.0.0.0:2112" ]
CMD [ "./startup.sh" ]


#docker run --rm -it -p 443:443 -p 80:80 -v $(pwd)/ssl/certs:/etc/ssl/certs -v $(pwd)/ssl/private:/etc/ssl/private https-test bash