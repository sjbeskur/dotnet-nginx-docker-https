﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace tmp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) {
			var config = new ConfigurationBuilder()  
				.AddCommandLine(args)  //server.urls http://localhost:5150
				//.SetBasePath(Directory.GetCurrentDirectory())
				//.AddJsonFile("hosting.json", optional: true)
				.Build();

            return WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(config)
                .UseStartup<Startup>()
                //.UseUrls("http://127.0.0.1:5000")
                .Build();
        }
    }
}
