﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace tmp.Controllers{
    [Route("api/[controller]")]
    public class ValuesController : Controller{

        private readonly ILogger _log;
        public ValuesController(ILogger<ValuesController> logger){
            _log = logger;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get(){
            _log.LogTrace("Trace");
            _log.LogInformation("fetching values from GET");
            _log.LogDebug("Debug");
            _log.LogWarning("Warning");
            _log.LogError("Error");
            _log.LogCritical("Critical");
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id){
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value){
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value){
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id){
        }
    }
}
